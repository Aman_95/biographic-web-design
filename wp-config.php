<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bethem');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S3gEzCXEZsI]i<0h4tGptZP87xpz_2==7ll9{NmlrJY;Sl8A,y><$7e^IqLlJ;A!');
define('SECURE_AUTH_KEY',  'j<DI(-e`rgCK0M_K:%UJvf_j_PW}{zJ62lW$lEsn}=R^s/e;o&m@NZOA@x~TyU^5');
define('LOGGED_IN_KEY',    '<t?1JHqkrp(e~s8{!ri_Oj4yUp]PkmNc/1S,rt/Sd?`pap&W97gR[ bHvG_+]pf6');
define('NONCE_KEY',        '%l>w]P{oAHAajCo.k`gnwZ[e!Smj1T!KzuaMNalzV>dG{lzVMu;X `wFGHBi> f]');
define('AUTH_SALT',        'CAd5[fxD`re3e{&aBT-rRolI/!R85y{HK>791*`Cnth0D^V]wUv-s)u)^oF[[8>X');
define('SECURE_AUTH_SALT', 'eLFzampN`v$`|Q`)Iw7u%hG7 &*uR.2<j]@$)O7H/[Fc~wh: [N~}Iqv~YhK]#:l');
define('LOGGED_IN_SALT',   'PtMfr2C+vE!M}| Ie_b7<U^piLh<a;o0r&vW)PX&3CypGy>~_<{)p7M}[tpp/1Mt');
define('NONCE_SALT',       'bU+@!xA5S;_w]{>^{Ox=^^X60T|KBuo|<,GxDlLXbL.V.7B|nMQ|3LX[WsEM7lP~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
