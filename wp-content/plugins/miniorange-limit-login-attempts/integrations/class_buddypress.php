<?php
	
	class Mo_BuddyPress{

        public static function signup_errors() {
            if (!isset($_POST['signup_username']))
                return;

            $user_name = $_POST['signup_username'];
            $error = "";
            global $bp;
            // making sure we are in the registration page
            if ( !function_exists('bp_is_current_component') || !bp_is_current_component('register') ) {
                return;
            }

            if (get_option('mo_wpns_enable_brute_force')) {
                $userIp = Mo_LLA_Util::get_client_ip();
                $mo_wpns_config = new Mo_LLA_Handler();
                $mo_wpns_config->add_transactions($userIp, $user_name, Mo_LLA_Constants::REGISTRATION_TRANSACTION, Mo_LLA_Constants::FAILED);

                $isWhitelisted = $mo_wpns_config->is_whitelisted($userIp);
                if(!$isWhitelisted){
                    $failedAttempts = $mo_wpns_config->get_failed_attempts_count($userIp);

                    //Slow Down
                    if(get_option('mo_wpns_slow_down_attacks')){
                        session_start();
                        if(isset($_SESSION["mo_wpns_failed_attepmts"]) && is_numeric($_SESSION["mo_wpns_failed_attepmts"]))
                            $_SESSION["mo_wpns_failed_attepmts"] += 1;
                        else
                            $_SESSION["mo_wpns_failed_attepmts"] = 1;
                        $mo_wpns_slow_down_attacks_delay = 2;
                        if(get_option('mo_wpns_slow_down_attacks_delay'))
                            $mo_wpns_slow_down_attacks_delay = get_option('mo_wpns_slow_down_attacks_delay');
                        sleep($_SESSION["mo_wpns_failed_attepmts"]*$mo_wpns_slow_down_attacks_delay);
                    }


                    $allowedLoginAttepts = 5;
                    if(get_option('mo_wpns_allwed_login_attempts'))
                        $allowedLoginAttepts = get_option('mo_wpns_allwed_login_attempts');

                    if(get_option('mo_wpns_enable_unusual_activity_email_to_user'))
                        $mo_wpns_config->sendNotificationToUserForUnusualActivities($user_name, $userIp, Mo_LLA_Messages::FAILED_REGISTRATION_ATTEMPTS_FROM_NEW_IP);

                    if($allowedLoginAttepts - $failedAttempts<=0){
                        $mo_wpns_config->block_ip($userIp, Mo_LLA_Messages::LOGIN_ATTEMPTS_EXCEEDED, false);
                        if(get_option('mo_wpns_enable_ip_blocked_email_to_admin'))
                            $mo_wpns_config->sendIpBlockedNotification($userIp,Mo_LLA_Messages::LOGIN_ATTEMPTS_EXCEEDED);
                        require_once '../templates/403.php';
                        exit();
                    }else {
                        if(get_option('mo_wpns_show_remaining_attempts')){
                            $diff = $allowedLoginAttepts - $failedAttempts;
                            $error = "<br>You have <b>".$diff."</b> attempts remaining.";
                        }
                    }
                }
            }

            if (get_option('mo_wpns_activate_recaptcha_for_buddypress_registration')) {
                $mo_wpns_recaptcha_handler = new Mo_LLA_Recaptcha_Handler();
                if (!$mo_wpns_recaptcha_handler->verify()) {
                    if (!isset($bp->signup->errors)) {
                        $bp->signup->errors = array();
                    }
                    $message = "Invalid captcha. Please verify captcha again.\n$error";
                    $bp->signup->errors['recaptcha_error'] = __('Invalid captcha. Please verify captcha again.');
                    bp_core_add_message($message, 'error');
                } else {
                    bp_core_add_message($error, 'error');
                }
            } else {
                bp_core_add_message($error, 'error');
            }
        }
	}

?>